TypeScript Templates
====================

TypeScript template projects using pnpm.

Ad Hoc
------

A TypeScript browser application with an ad hoc build system.

- Linting with ESLint extending Airbnb, Airbnb TypeScript, TypeScript recommended,
  and Prettier configs.
- Formatting with Prettier.
- Ad hoc build script to transpile TypeScript, and copy static files.
- Dev script to run a simple HTTP server. The project should be built before running
  this script.

Vite
----

A TypeScript browser application built with Vite.

- Linting with ESLint extending Airbnb, Airbnb TypeScript, TypeScript recommended,
  and Prettier configs.
- Formatting with Prettier.
- Modify stock Vite files to pass lint rules.
  - Remove `.ts` from imports.
  - Make the single export in `counter.ts` a default export.
  - Ignore `no-param-reassing` rule in `counter.ts`.
