const a = "foo" + "bar" // eslint-config-airbnb-base error
const b: String = "baz" // @typescript-eslint error
const c = 'quo' // eslint-plugin-prettier error

export default function foo(): number {
  return a + b + c // typescript error
}

const h = document.createElement("h1")
h.innerHTML = "TypeScript App"
document.getElementById("app")?.appendChild(h)
