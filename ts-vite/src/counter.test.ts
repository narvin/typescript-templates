import { describe, test, expect } from "vitest"
import * as counter from "./counter"

describe("inc", () => {
  test("adds 1 to the input", () => {
    expect(counter.inc(0)).toBe(1)
  })
})
