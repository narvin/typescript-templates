export function inc(x: number): number {
  return x + 1
}

export default function setupCounter(element: HTMLButtonElement) {
  let counter = 0
  const setCounter = (count: number) => {
    counter = count
    // eslint-disable-next-line no-param-reassign
    element.innerHTML = `count is ${counter} foo`
  }
  element.addEventListener("click", () => setCounter(inc(counter)))
  setCounter(0)
}
